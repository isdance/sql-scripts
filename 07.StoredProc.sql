-- Create Procedure spGetEmployees
-- as
-- Begin
--   SELECT [Name], ManagerID 
--   FROM tblEmployee
-- End

-- Create Procedure spGetEmployeesByGenderAndDepartment 
-- @Gender NVARCHAR(50),
-- @DepartmentId INT
-- AS
-- BEGIN
--   SELECT Name, Gender 
--   FROM tblEmployee 
--   WHERE Gender = @Gender 
--   AND DepartmentId = @DepartmentId
-- END

-- EXEC spGetEmployeesByGenderAndDepartment 'Male', 1

Create Procedure spGetEmployeeCountByGender
@Gender nvarchar(20),
@EmployeeCount int Output
as
Begin
 Select @EmployeeCount = COUNT(Id) 
 from tblEmployee 
 where Gender = @Gender
End 

Declare @EmployeeTotal int -- 定义一个变量，准备接受输出参数的值
Execute spGetEmployeeCountByGender 'Female', @EmployeeTotal output
Print @EmployeeTotal

exec sp_help spGetEmployeeCountByGender
