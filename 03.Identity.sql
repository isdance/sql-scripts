﻿select * from tblPersonNew

Create Table tblPersonNew
(
	PersonId int Identity(1,1) Primary Key,
	Name nvarchar(20)
)

INSERT INTO dbo.tblPersonNew(Name) VALUES('Jeremy')
SET IDENTITY_INSERT dbo.tblPersonNew ON
INSERT INTO dbo.tblPersonNew(PersonId,Name) VALUES(2, 'Jeremy')

SET IDENTITY_INSERT dbo.tblPersonNew ON
INSERT INTO dbo.tblPersonNew(PersonId,Name) VALUES(3, 'Jeremy')
SELECT SCOPE_IDENTITY() 

Alter Table dbo.tblPersonNew
Add Constraint UQ_Name Unique("Name")