CREATE TABLE tblDepartment
(
  Id int IDENTITY(1,1) PRIMARY KEY,
  DepartmentName VARCHAR(100) NOT NULL,
  [Location] VARCHAR(100) NOT NULL,
  DepartmentHead VARCHAR(100) NOT NULL
)

CREATE TABLE tblEmployee
(
  Id int IDENTITY(1,1) PRIMARY KEY,
  [Name] VARCHAR(100) NOT NULL,
  Gender VARCHAR(10) NOT NULL,
  Salary MONEY NOT NULL,
  DepartmentId int FOREIGN KEY REFERENCES tblDepartment(Id)
)

INSERT INTO tblDepartment VALUES('IT', 'London', 'Rick')
INSERT INTO tblDepartment VALUES('Payroll', 'Delhi', 'Ron')
INSERT INTO tblDepartment VALUES('HR', 'New York', 'Christie')
INSERT INTO tblDepartment VALUES('Other Department', 'Sydney', 'Cindrella')

INSERT INTO tblEmployee VALUES('Tom', 'Male', 4000, 1)
INSERT INTO tblEmployee VALUES('Pam', 'Female', 3000, 3)
INSERT INTO tblEmployee VALUES('John', 'Male', 3500, 1)
INSERT INTO tblEmployee VALUES('Sam', 'Male', 4500, 2)
INSERT INTO tblEmployee VALUES('Todd', 'Male', 2800, 2)
INSERT INTO tblEmployee VALUES('Ben', 'Male', 7000, 1)
INSERT INTO tblEmployee VALUES('Sara', 'Female', 4800, 3)
INSERT INTO tblEmployee VALUES('Valarie', 'Female', 5500, 1)
INSERT INTO tblEmployee VALUES('James', 'Male', 6500, NULL)
INSERT INTO tblEmployee VALUES('Russell', 'Male', 8800, NULL)

-- INNER JOIN
SELECT Name, Gender, Salary, DepartmentName
FROM tblEmployee
INNER JOIN tblDepartment
ON tblEmployee.DepartmentId = tblDepartment.Id

-- LEFT JOIN
SELECT Name, Gender, Salary, DepartmentName
FROM tblEmployee 
LEFT JOIN tblDepartment
ON tblEmployee.DepartmentId = tblDepartment.Id

-- RIGHT JOIN
SELECT Name, Gender, Salary, DepartmentName
FROM tblEmployee
RIGHT JOIN tblDepartment
ON tblEmployee.DepartmentId = tblDepartment.Id

-- FULL JOIN
SELECT Name, Gender, Salary, DepartmentName
FROM tblEmployee
FULL JOIN tblDepartment
ON tblEmployee.DepartmentId = tblDepartment.Id






