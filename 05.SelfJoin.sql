CREATE TABLE tblEmployee
(
  EmployeeID int IDENTITY PRIMARY KEY,
  [Name] VARCHAR(100) NOT NULL,
  ManagerID int
)

INSERT INTO tblEmployee VALUES('Mike', 3)
INSERT INTO tblEmployee VALUES('Rob', 1)
INSERT INTO tblEmployee VALUES('Todd', NULL)
INSERT INTO tblEmployee VALUES('Ben', 1)
INSERT INTO tblEmployee VALUES('Sam', 1)

SELECT * FROM tblEmployee

SELECT e.NAME AS Employee, m.Name As Manager
FROM tblEmployee AS e
LEFT JOIN tblEmployee AS m
ON e.ManagerID = m.EmployeeID

SELECT E.Name as Employee, ISNULL(M.Name,'No Manager') as Manager
FROM tblEmployee E
LEFT JOIN tblEmployee M
ON E.ManagerID = M.EmployeeID

SELECT E.Name as Employee, CASE WHEN M.Name IS NULL THEN 'No Manager' 
   ELSE M.Name END as Manager
FROM  tblEmployee E
LEFT JOIN tblEmployee M
ON   E.ManagerID = M.EmployeeID

SELECT E.Name as Employee, COALESCE(M.Name, 'No Manager') as Manager
FROM tblEmployee E
LEFT JOIN tblEmployee M
ON E.ManagerID = M.EmployeeID