CREATE TABLE tblUSEmployee
(
  Id int IDENTITY PRIMARY KEY,
  NAME VARCHAR(100) NOT NULL,
  EMAIL VARCHAR(100) NOT NULL
)

CREATE TABLE tblAUEmployee
(
  Id int IDENTITY PRIMARY KEY,
  NAME VARCHAR(100) NOT NULL,
  EMAIL VARCHAR(100) NOT NULL
)

INSERT INTO tblUSEmployee VALUES('Raj', 'R@R.com')
INSERT INTO tblUSEmployee VALUES('Sam', 'S@S.com')

INSERT INTO tblAUEmployee VALUES('Ben', 'B@B.com')
INSERT INTO tblAUEmployee VALUES('Sam', 'S@S.com')

SELECT Id, Name, Email FROM tblUSEmployee
UNION 
SELECT Id, Name, Email FROM tblAUEmployee

SELECT Id, Name, Email FROM tblUSEmployee
UNION ALL
SELECT Id, Name, Email FROM tblAUEmployee